Use the following commands: 

```
git clone git@bitbucket.org:aghbit/oop-study-group.git
git checkout -b "ImieNazwisko"
echo "first file" > myfirstfile.txt
git add myfirstfile.txt
git commit -m "initial commit for ImieNazwisko"
git push origin ImieNazwisko
git checkout -b "Zadanie 1"
(... code the task here... then push the changes ... and create pull request - in case of problems use hipchat or googlegroup for troubleshooting :P)
```

